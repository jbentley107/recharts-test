/**
 * @file App.js
 */
// Import dependencies
import React from 'react';
import logo from './logo.svg';
import './App.css';

// Import components
import Chart from './Chart';

function App() {
  return (
    <div className="App">     
      <h1>Recharts Example</h1>
      <Chart />      
    </div>
  );
}

export default App;
